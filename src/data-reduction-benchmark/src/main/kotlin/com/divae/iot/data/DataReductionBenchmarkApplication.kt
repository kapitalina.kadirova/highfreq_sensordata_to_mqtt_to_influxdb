package com.divae.iot.data

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DataReductionBenchmarkApplication

fun main(args: Array<String>) {
    runApplication<DataReductionBenchmarkApplication>(*args)
}
