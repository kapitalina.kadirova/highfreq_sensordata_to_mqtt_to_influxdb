package com.divae.iot.data.config

import com.divae.iot.data.influxdb.InfluxDbTestDataRepository
import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class InfluxDbConfig {

    @Bean
    fun influxDbRepository(driver: InfluxDB) = InfluxDbTestDataRepository(driver)

    @Bean
    fun driver(@Value("\${influxdb.url}") url: String): InfluxDB {
        return InfluxDBFactory.connect(url)
    }
}