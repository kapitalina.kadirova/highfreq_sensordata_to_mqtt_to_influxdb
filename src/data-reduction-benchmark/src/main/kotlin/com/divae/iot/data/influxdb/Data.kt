package com.divae.iot.data.influxdb

import org.influxdb.annotation.Column
import org.influxdb.annotation.Measurement
import org.influxdb.annotation.TimeColumn
import java.time.Instant


@Measurement(name = "data")
data class Data(@TimeColumn @Column(name = "time") val time: Instant?, @Column(name = "value") val value: Double?) {
    constructor(): this(null, null)
}