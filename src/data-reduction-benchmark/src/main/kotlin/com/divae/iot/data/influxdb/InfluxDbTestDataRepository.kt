package com.divae.iot.data.influxdb

import org.influxdb.InfluxDB
import org.influxdb.dto.BatchPoints
import org.influxdb.dto.Point
import org.influxdb.dto.Query
import org.influxdb.impl.InfluxDBResultMapper
import org.slf4j.LoggerFactory
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.concurrent.TimeUnit

class InfluxDbTestDataRepository(private val driver: InfluxDB) {

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val log = LoggerFactory.getLogger(javaClass.enclosingClass)
    }

    private val resultMapper = InfluxDBResultMapper()

    fun read(): Flux<Data> {
        val result = driver.query(Query("select * from data", "test_data"))

        return Flux.fromStream(resultMapper.toPOJO(result, Data::class.java).stream())
    }

    fun generate(count: Int): Mono<String> {

        val points = ArrayList<Point>()
        for (i in 0..count) {
            points.add(
                    Point.measurement("data")
                    .time(System.nanoTime(), TimeUnit.NANOSECONDS)
                    .addField("value", i.toDouble())
                    .build())
        }

        val batchPoints = BatchPoints.database("test_data")
                .retentionPolicy("autogen")
                .points(points)
                .build()

        driver.write(batchPoints)

        return Mono.just("OK")
    }

    fun delete(): Mono<Void> {
        val result = driver.query(Query("delete from data", "test_data"))

        if (result.hasError()) {
            log.error(result.error)
        }
        return Mono.just(result.hasError()).then()
    }
}