package com.divae.iot.data.web

import com.divae.iot.data.influxdb.Data
import com.divae.iot.data.influxdb.InfluxDbTestDataRepository
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/data")
class DataResource(private val repository: InfluxDbTestDataRepository) {

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val log = LoggerFactory.getLogger(javaClass.enclosingClass)
    }

    @PostMapping
    fun generateData(@RequestBody count: Int): Mono<String> {
        log.info("Creating sample data with count: [$count]")
        return repository.generate(count)
    }

    @GetMapping
    fun getData(): Flux<Data> = repository.read()

    @DeleteMapping
    fun deleteData(): Mono<Void> = repository.delete()
}