import smartMillingMachine.MmProcessing;
import smartParkingLot.PlProcessing;

public class MainClass {
    public static void main(String[] args) throws Exception {
        String connection = "tcp://127.0.0.1:1883";

        Thread millingMachine = new Thread(new MmProcessing(connection));
        Thread parkingLot = new Thread(new PlProcessing(connection));

        millingMachine.start();
        parkingLot.start();
    }
}