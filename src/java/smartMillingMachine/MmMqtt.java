package smartMillingMachine;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import java.util.UUID;

public class MmMqtt {
    private IMqttClient client;

    public MmMqtt(String connection) throws MqttException {
        String publisherId = UUID.randomUUID().toString();
        this.client = new MqttClient(connection, publisherId);

        mqttOptions();
    }

    public IMqttClient getClient() {
        return this.client;
    }

    private void mqttOptions() throws MqttException {
        MqttConnectOptions options = new MqttConnectOptions();

        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);

        this.client.connect(options);
    }
}
