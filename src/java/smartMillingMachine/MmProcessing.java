package smartMillingMachine;

public class MmProcessing implements Runnable {
    MmMqtt mqtt;
    MmTempSensor tempSensor;

    public MmProcessing(String connection) throws Exception {
        this.mqtt = new MmMqtt(connection);
        this.tempSensor = new MmTempSensor(mqtt.getClient());
    }

    public void start() {
        while (true) {
            try {
                tempSensor.publish();

                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void run() {
        start();
    }
}
