package smartMillingMachine;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Random;

public class MmTempSensor {
    private final String TOPIC = "millingMachine/temp";
    private IMqttClient client;
    private Random random = new Random();
    private float randomTemp = 0;

    public MmTempSensor(IMqttClient client) {
        this.client = client;
    }

    public void publish() throws Exception {
        if ( this.client.isConnected()) {
            MqttMessage msg = getMessage();

            msg.setQos(0);
            msg.setRetained(false);
            this.client.publish(this.TOPIC, msg);
        }

    }

    private MqttMessage getMessage() {
        Timestamp messageTime = getTime();

        String json = ("{"
                + "\"measurement\":\"temp\","
                + "\"tags\":{"
                    + "\"system\":\"millingMachine\""
                + "},"
                + "\"time\":\"" + messageTime + "\","
                + "\"field\":{"
                    + "\"value\":\"" + getTemp() + "\""
                + "}"
                + "}");

        byte[] payload = json.getBytes(StandardCharsets.UTF_8);

        return new MqttMessage(payload);
    }

    private Timestamp getTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    private float getTemp() {
        if (this.randomTemp == 0) {
            this.randomTemp = random.nextFloat() * (60.0f - 10.0f) + 10.0f;
        } else if (this.randomTemp != 0) {
            if (this.random.nextBoolean()) {
                this.randomTemp = this.randomTemp + random.nextFloat() * (1.0f - 0.0f) + 0.0f;
            } else {
                this.randomTemp = this.randomTemp - random.nextFloat() * (1.0f - 0.0f) + 0.0f;
            }

        }

        return this.randomTemp;
    }

}
