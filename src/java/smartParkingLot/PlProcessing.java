package smartParkingLot;

public class PlProcessing implements Runnable {
    PlMqtt mqtt;
    PlStatusSensor statusSensor;

    public PlProcessing(String connection) throws Exception {
        this.mqtt = new PlMqtt(connection);
        this.statusSensor = new PlStatusSensor(mqtt.getClient());
    }

    public void start() {
        while (true) {
            try {
                statusSensor.publish();

                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void run() {
        start();
    }
}