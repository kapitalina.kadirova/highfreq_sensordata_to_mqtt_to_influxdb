package smartParkingLot;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Random;

public class PlStatusSensor {
    private final String TOPIC = "parkingLot/status";
    private IMqttClient client;
    private Random random = new Random();
    private boolean randomBool;
    private Timestamp refreshingTime;
    private final long INTERVAL = 1000 * 60 * 5;

    public PlStatusSensor(IMqttClient client) {
        this.client = client;
    }

    public void publish() throws Exception {
        if ( this.client.isConnected()) {
            MqttMessage msg = getMessage();

            msg.setQos(0);
            msg.setRetained(false);
            this.client.publish(this.TOPIC, msg);
        }

    }

    private MqttMessage getMessage() {
        Timestamp messageTime = getTime();

        if (refreshingTime == null) {
            this.randomBool = getStatus();
        } else if ((messageTime.getTime() - refreshingTime.getTime()) > INTERVAL) {
            this.randomBool = getStatus();
        }

        String json = ("{"
                + "\"measurement\":\"status\","
                + "\"tags\":{"
                    + "\"system\":\"parkingLot\""
                + "},"
                + "\"time\":\"" + messageTime + "\","
                + "\"field\":{"
                    + "\"value\":\"" + this.randomBool + "\""
                + "}"
                + "}");

        byte[] payload = json.getBytes(StandardCharsets.UTF_8);

        return new MqttMessage(payload);
    }

    private boolean getStatus() {
        this.refreshingTime = getTime();
        this.randomBool = this.random.nextBoolean();

        return this.randomBool;
    }

    private Timestamp getTime() {
        return new Timestamp(System.currentTimeMillis());
    }
}
