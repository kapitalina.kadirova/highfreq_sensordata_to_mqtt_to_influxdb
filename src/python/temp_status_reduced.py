# -*- coding: utf-8 -*-
"""
Date: 13.01.2020
Author: Kapitalina Hänsel
"""
# bash
#!/usr/bin/env python3

"""A MQTT to InfluxDB Bridge
This script receives MQTT data (Temperature and Status) and saves those to InfluxDB.
Along with full load  data reduction take place:
- For Temperature the mean value for each 300 data points is calculated 
- For Status only on-change values will be saved
"""

import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient
import json
import datetime
from statistics import mean

INFLUXDB_ADDRESS = 'localhost'
INFLUXDB_USER = 'root'
INFLUXDB_PASSWORD = 'root'
INFLUXDB_DATABASE = 'MM_DB'

MQTT_ADDRESS = 'localhost'
MQTT_USER = ''
MQTT_PASSWORD = ''
MQTT_TOPIC = 'millingMachine/temp'
MQTT_TOPIC2 = 'parkingLot/status'
MQTT_CLIENT_ID = ''

influxdb_client = InfluxDBClient(INFLUXDB_ADDRESS, 8086, INFLUXDB_USER, INFLUXDB_PASSWORD, None)

tempArray =[]
statusArray =[]

def on_connect(client, userdata, flags, rc):
    """ The callback for when the client receives a CONNACK response from the server."""
    client.subscribe(MQTT_TOPIC)
    client.subscribe(MQTT_TOPIC2)

def on_message(client, userdata, msg):
    """The callback for when a PUBLISH message is received from the server."""
    print(msg.topic + ' ' + str(msg.payload))
    print(type(msg.payload))
    data = msg.payload.decode('utf-8')
    myJson = json.loads(data)
    if msg.topic == 'millingMachine/temp':
        tempValue = myJson['fields']['value']
        print(type(tempValue))
        persists(tempValue)
        tempArray.append(float(tempValue))
        if len(tempArray)<=1:
            print('First value in meanTemp' + str(tempValue))
            persists_mean(tempValue)            
        else:
            print(str(tempArray))
            calc_mean(tempArray)
    elif msg.topic == 'parkingLot/status':
        statusValue = myJson['fields']['value']
        print(statusValue)
        print(type(statusValue))
        persists_stat(statusValue)
        on_change_status(statusValue)
        
def on_change_status(statusValue):
    """check if the status is changed"""
    print('!!!!!!!!!!!!!')
    if len(statusArray)<1:
        print('schreibe erstes Status')
        persists_new_stat(statusValue)
        statusArray.append(str(statusValue))
        print(str(statusArray))
    elif statusValue != statusArray[-1]:
        print('Status hat sich veraendert')
        persists_new_stat(statusArray[-1])
        persists_new_stat(statusValue)
        statusArray.append(str(statusValue))
        statusArray.pop(0)
        print(str(statusArray))
    elif statusValue == statusArray[-1]:
        print('Status eandert sich nicht')
        print(str(statusArray))
        

def calc_mean(tempArray):
    """calculate average value for the each 300 data points"""
    print('***********')
    if len(tempArray)>300:
        meanTemp = mean(tempArray)
        print(meanTemp)
        print(type(meanTemp))
        del tempArray[:]
        print('ende')
        persists_mean(meanTemp)
        

        
def persists_mean(meanTemp):
    """save the meanTemp value to InfluxDB"""
    print(str(meanTemp))
    current_time = datetime.datetime.utcnow().isoformat()
    print("test8 "+str(meanTemp))
    json_body = [
        {
            "measurement": "meanTemp",
            "tags": {
                "sensor": "tempSim1"
                },
            "time": current_time,
            "fields": {
                "value": meanTemp
            }
        }
    ]
    print('test7')
    influxdb_client.write_points(json_body)
   
       
def persists(tempValue):
    """save the all recieved temperature data to InfluxDB"""
    current_time = datetime.datetime.utcnow().isoformat()
    print("test3 "+tempValue)
    json_body = [
        {
            "measurement": "temp",
            "tags": {
                "sensor": "tempSim1"
                },
            "time": current_time,
            "fields": {
                "value": float(tempValue)
            }
        }
    ]
    print('test6')
    influxdb_client.write_points(json_body)
   

    
def persists_stat(statusValue):
    """save the all recieved status data to InfluxDB"""
    current_time = datetime.datetime.utcnow().isoformat()
    print("test 4"+statusValue)
    if statusValue =='true':
        statusValue = 1
    elif statusValue == 'false':
        statusValue = 0
    print(str(statusValue))
    json_body = [
        {
            "measurement": "status",
            "tags": {
                "sensor": "statSim1"
                },
            "time": current_time,
            "fields": {
                "value": statusValue
            }
        }
    ]
    influxdb_client.write_points(json_body)
    
def persists_new_stat(statusValue):
    """save the on-changed status data to InfluxDB"""
    current_time = datetime.datetime.utcnow().isoformat()
    print("test 9"+statusValue)
    if statusValue =='true':
        statusValue = 1
    elif statusValue == 'false':
        statusValue = 0
    print(str(statusValue))
    json_body = [
        {
            "measurement": "statusChanged",
            "tags": {
                "sensor": "statSim1"
                },
            "time": current_time,
            "fields": {
                "value": statusValue
            }
        }
    ]
    influxdb_client.write_points(json_body)

def _init_influxdb_database():
    databases = influxdb_client.get_list_database()
    if len(list(filter(lambda x: x['name'] == INFLUXDB_DATABASE, databases))) == 0:
        influxdb_client.create_database(INFLUXDB_DATABASE)
    influxdb_client.switch_database(INFLUXDB_DATABASE)


def main():
    _init_influxdb_database()

    mqtt_client = mqtt.Client(MQTT_CLIENT_ID)
    mqtt_client.username_pw_set(MQTT_USER, MQTT_PASSWORD)
    mqtt_client.on_connect = on_connect
    mqtt_client.on_message = on_message

    mqtt_client.connect(MQTT_ADDRESS, 1883)
    mqtt_client.loop_forever()


if __name__ == '__main__':
    print('MQTT to InfluxDB bridge')
    main()



